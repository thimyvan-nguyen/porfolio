<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css" integrity="sha384-v2Tw72dyUXeU3y4aM2Y0tBJQkGfplr39mxZqlTBDUZAb9BGoC40+rdFCG0m10lXk" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css" integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="<?php echo get_bloginfo('template_url'); ?>/assets/css/main.css" rel="stylesheet" type="text/css" media="screen" />
<script src="<?php echo get_bloginfo('template_url'); ?>/assets/js/main.js"></script>
</head>

<body <?php body_class(); ?> >
<div id="page" class="site">
	<header class="header modal-move fixed collapsed color-adapt light">
		<div class="blocks vertical">
			<div class="block">
				<a href="/" class="logo" data-target="home">
					<span>V<span class="collapse"><i>a</i><i>n</i></span>.</span>
				</a>
			</div>
			<a class="main-link directional-link detail" href='/'>About me<?php echo porfolio_get_svg( array( 'icon' => 'arrow-right' ) ); ?></a>
			
		<?php if (!is_front_page() && !is_single()){
			echo '<h1 class="page-title">My Work</h1>';
			echo '<ul class="categories">';
			$args = array(    
				'show_option_all'    => 'All',
				'current_category'   => 0,
				'hide_empty'         => 1,
				'title_li'           => '',
				'orderby'            => 'name',
    		'order'              => 'DESC',
				);
				wp_list_categories($args);
			echo '</ul>';
		}?>
		
		<?php if (is_single()){
			echo '<h1 class="page-title">';
			echo porfolio_get_svg( array( 'icon' => 'arrow-left' ) );
			echo ' <a href="/works">My Work</a></h1>';
		}?>
		</div>
	</header>