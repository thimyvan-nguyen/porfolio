<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<main id="main" class="site-main" role="main">
	

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/post/content-single', get_post_format() ); ?>
				<div class="container">
					<div class="row">
				<?php the_post_navigation( array(
					'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'porfolio' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'porfolio' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . porfolio_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'porfolio' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'porfolio' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . porfolio_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',
				) ); ?>
			</div><!-- .wrap -->
			</div>
			<?php
			endwhile; // End of the loop.
			?>

		
</main><!-- #main -->
<?php get_footer();
