<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
		<div class="col-xs-12 col-sm-4">
				<div id="post-<?php the_ID(); ?>" class="card card__classdojo <?php echo the_field('class') ?>" >
					<a href="<?php the_permalink(); ?>" class="card__link"></a>
					<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
					<?php the_post_thumbnail('card__image' ); ?>
					<?php endif; ?>
					<?php the_title( '<h1 class="card__title">', '</h1>' ); ?>
					<p class="card__tags"><?php the_tags('') ?></p>
				<div class="entry-content">
				<?php	
					wp_link_pages( array(
						'before'      => '<div class="page-links">' . __( 'Pages:', 'porfolio' ),
						'after'       => '</div>',
						'link_before' => '<span class="page-number">',
						'link_after'  => '</span>',
					) );
					?>
				</div><!-- .entry-content -->

				<?php
				if ( is_single() ) {
					porfolio_entry_footer();
				}
				?>
			</div>
</div>			
	
