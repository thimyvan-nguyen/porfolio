<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
		<div>
				<div id="post-<?php the_ID(); ?>" class="card card__classdojo">
					<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
					<?php the_post_thumbnail('card__image' ); ?>
					<?php endif; ?>
					<?php the_title( '<h1 class="card__title">', '</h1>' ); ?>
					<?php the_content() ?>;
				<div class="entry-content">
				<?php	
					wp_link_pages( array(
						'before'      => '<div class="page-links">' . __( 'Pages:', 'porfolio' ),
						'after'       => '</div>',
						'link_before' => '<span class="page-number">',
						'link_after'  => '</span>',
					) );
					?>
				</div><!-- .entry-content -->
			</div>
</div>			
	
