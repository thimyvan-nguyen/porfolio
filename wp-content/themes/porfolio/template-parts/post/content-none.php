<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<section class="no-results not-found">
	<div class="page-content">
			<p class="text-center"><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for.', 'porfolio' ); ?></p>
	</div>
</section>
