<?php /*** Displays content for front page */ ?>
<section id="post-<?php the_ID(); ?>" <?php post_class( 'porfolio-panel ' ); ?> >
	<div class="content">
		<div class="blocks">
			<div class="block half">
					<?php the_content( sprintf(__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'porfolio' ),get_the_title()) ); ?>
			</div>
			<div class="block half">
				<div class="content">
					<a class="red" target="_blank" href="/wp-content/uploads/2016/02/Resume.pdf">Download my resume.</a>
				</div>
			</div>
		</div>
		</div>
</section>