	<?php
	/**
	 * The template for displaying archive pages
	 *
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package WordPress
	 * @subpackage Twenty_Seventeen
	 * @since 1.0
	 * @version 1.0
	 */

	get_header(); ?>

	<main id="main" class="site-main" role="main">
		<div class="container">
			<div class="row">
						<?php
				if ( have_posts() ) :

					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/post/content', get_post_format() );

					endwhile;

					the_posts_pagination( array(
						'prev_text' => porfolio_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'porfolio' ) . '</span>',
						'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'porfolio' ) . '</span>' . porfolio_get_svg( array( 'icon' => 'arrow-right' ) ),
						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'porfolio' ) . ' </span>',
					) );

				else :

					get_template_part( 'template-parts/post/content', 'none' );

				endif;
				?>
			</div>
		</div>
	</main><!-- #main -->



	<?php get_footer();
