<?php
/** * The front page template file  */
get_header(); ?>
	<div id="masthead" class="site-header" role="banner">
		<div class="custom-header">
			<div class="custom-header-media">
				<?php the_custom_header_markup(); ?>
			</div>
		</div>
		<div class="site-branding">
				<div class="blocks align-center">
					<div class="block">
						<div class="home content">
							<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
							<?php $description = get_bloginfo( 'description', 'display' );
								if ( $description || is_customize_preview() ) :
								?>
								<p class="site-description"><?php echo $description; ?></p>
							<?php endif; ?>
                            <p class="site-description" style="max-width:450px;margin-top:20px"><img src="/wp-content/uploads/2019/03/tittle.png" style="width:100%"></p>
							<p class=""><a href="mailto:myvan0908@gmail.com" target="_blank" class="">myvan0908@gmail.com</a></p>
						</div>
					</div>
				</div>
				<a href="#content" class="learn-more">Learn More<?php echo porfolio_get_svg( array( 'icon' => 'arrow-right' ) ); ?></a>
		</div>
	</div>
	<a class="main-link directional-link" href="/works">View my work<?php echo porfolio_get_svg( array( 'icon' => 'arrow-right' ) ); ?></a>
	<div class="site-content-contain">
	<div id="content" class="site-content">
			<div id="primary" class="content-area">
					<?php 
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/page/content', 'front-page' );
						endwhile;
					else :
						get_template_part( 'template-parts/post/content', 'none' );
					endif; ?>
			</div>

<?php get_footer();
